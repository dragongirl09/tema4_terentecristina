import React, { Component } from "react";

export default class RobotForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      type: "",
      mass: ""
    };
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  add = () => {
    this.props.onAdd(this.state.name, this.state.type, this.state.mass);
  };
  render() {
    return (
      <form>
        <input type="text" id="name" name="name" onChange={this.handleChange} />
        <input type="text" id="type" name="type" onChange={this.handleChange} />
        <input type="text" id="mass" name="mass" onChange={this.handleChange} />
        <input type="button" value="add" onClick={this.add} />
      </form>
    );
  }
}
